﻿using Newtonsoft.Json;

namespace TaskEight
{
    public class NewtonsoftSerializer : ISerializer
    {
        public string SerializeToString<T>(T obj) where T : new()
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings() { });
        }
        public T DeserializeFromString<T>(string str) where T : new()
        {
            return JsonConvert.DeserializeObject<T>(str);
        }     
    }
}
