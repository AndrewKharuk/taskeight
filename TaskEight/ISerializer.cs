﻿namespace TaskEight
{
    public interface ISerializer
    {
        string SerializeToString<T>(T obj) where T : new();

        T DeserializeFromString<T>(string str) where T : new();
    }
}
