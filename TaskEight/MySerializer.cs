﻿using System;
using System.Reflection;
using System.Text;

namespace TaskEight
{
    public class MySerializer : ISerializer
    {
        public string SerializeToString<T>(T obj) where T : new()
        {
            Type myType = obj.GetType();

            FieldInfo[] allFields = myType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            StringBuilder result = new StringBuilder(string.Empty);
            foreach (var field in allFields)
            {
                result.Append($"{field.Name.ToString()}&{field.GetValue(obj).ToString()} ");
            }

            return result.ToString().Trim();
        }

        public T DeserializeFromString<T>(string str) where T : new()
        {
            string[] fields = str.Split(' ');

            T obj = new T(); 

            Type typeOfObj = obj.GetType();

            foreach (var item in fields)
            {
                string[] nameAndValue = item.Split('&');
              
                typeOfObj.GetField(nameAndValue[0], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(obj, Int32.Parse(nameAndValue[1]));
            }
            
            return obj;
        }  
    }
}
