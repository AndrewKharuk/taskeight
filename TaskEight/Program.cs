﻿using System;

namespace TaskEight
{
    class Program
    {
        static void Main(string[] args)
        {
            F testObj = F.Get();

            GetTestResult(new MySerializer(), testObj, 1000);
            GetTestResult(new NewtonsoftSerializer(), testObj, 1000);
            GetTestResult(new MySerializer(), testObj, 100000);
            GetTestResult(new NewtonsoftSerializer(), testObj, 100000);

            Console.ReadKey();
        }


        private static void GetTestResult<T>(ISerializer serializer, T testObject, int iterations) where T : new()
        {
            string result = string.Empty;

            var startTime = System.Diagnostics.Stopwatch.StartNew();

            for (int i = 0; i < iterations; i++)
            {
                result = serializer.SerializeToString<T>(testObject);
            }

            startTime.Stop();
            var resultTime = startTime.Elapsed;

            string serializeTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                resultTime.Hours,
                resultTime.Minutes,
                resultTime.Seconds,
                resultTime.Milliseconds);

            startTime = System.Diagnostics.Stopwatch.StartNew();

            for (int i = 0; i < iterations; i++)
            {
                T obj = serializer.DeserializeFromString<T>(result);
            }

            startTime.Stop();
            resultTime = startTime.Elapsed;

            string deserializeTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                resultTime.Hours,
                resultTime.Minutes,
                resultTime.Seconds,
                resultTime.Milliseconds);

            Console.WriteLine($"Сериализатор: {serializer.GetType()}\r\nТип объекта: {testObject.GetType()}\r\nколичество итераций: {iterations}\r\nвремя сериализации: {serializeTime}\r\nстрока: {result}\r\nвремя десериализации: {deserializeTime}\r\n");
            Console.WriteLine("=========================");
        }
    }

    class F
    {
        [Newtonsoft.Json.JsonRequired]
        int i1, i2, i3, i4, i5;
        public static F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
    }
}
